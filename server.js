const express = require("express");
const app = express();
const mongoose = require("mongoose");
const config = require("./config");
const cors = require('cors');
const multer = require('multer');
const stripe = require('stripe')('sk_test_ZDr6MA9S8WiHQT7x9ge9TjX500oMfQNSRC')

// define storage
let storage = multer.diskStorage({
	destination: (req, file, cb)=>{
		cb(null, 'public/images/uploads')
	},
	filename: (req, file, cb)=>{
		cb(null, Date.now() + "-" +file.originalname)
	}
});

const upload = multer({storage});

const path = require('path');
app.use(express.static(path.join(__dirname, 'public')))

const databaseUrl = process.env.DATABASE_URL || "mongodb+srv://admin:Acts238@cluster0-jmy61.mongodb.net/exercise?retryWrites=true&w=majority"
mongoose.connect(databaseUrl,{
	useNewUrlParser:true, 
	useUnifiedTopology:true, 
	useFindAndModify:false,
	useCreateIndex: true
}).then(()=>{
	console.log("Remote Database Connection Established");
});

app.use(express.urlencoded({extended:false}));
app.use(express.json());
app.use(cors());

app.listen(config.port, () => {
	console.log(`Listening on Port ${config.port}`);
});

// asset api
const assets = require("./routes/assets_router");
app.use('/admin', assets);

const requests = require("./routes/request_router");
app.use('/admin', requests);

const users = require("./routes/users_router");
app.use('/', users);

const auth = require("./routes/auth_router");
app.use('/', auth);

const vehicles = require('./routes/vehicles_router');
app.use('/admin', vehicles);

const requestedvehicle = require("./routes/requestmodel_router");
app.use('/admin', requestedvehicle);

app.post('/upload', upload.single('image'), (req, res)=>{
	if(req.file){
		res.json({imageUrl: `images/uploads/${req.file.filename}`});
	}else{
		res.status("409").json('No Files to Upload')
	}
})

app.post('/charge', (req, res)=> {
	console.log(req.body)
	try{
		
		stripe.customers.create({
			email: req.body.email,
			description: req.body.description,
			source: "tok_visa",
		}).then(customers=>
			stripe.charges.create({
				amount: req.body.amount,
				currency: "php",
				source: "tok_visa",
				description: req.body.description,
			})
		).then(result=>{
			res.send(result);
			console.log(result)
		}
		)
	}catch(e){
		console.log(e)
	}
})