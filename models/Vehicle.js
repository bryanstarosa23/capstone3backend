const mongoose = require('mongoose');
const Schema=mongoose.Schema;

const VehicleSchema = new Schema({
    name: String,
    description: String,
    plateNumber: { type: String, unique: true},
    category: String,
    rentPrice:Number, 
    image:String,
    code:String
    
});

module.exports = mongoose.model("Vehicle", VehicleSchema);