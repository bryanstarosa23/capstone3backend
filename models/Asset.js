const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AssetSchema = new Schema({
	name: String,
	description: String,
	serialNumber: String,
	category: String,
	stock: Number
});

module.exports = mongoose.model("Asset", AssetSchema);



