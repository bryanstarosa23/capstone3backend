const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const moment = require('moment');

const RequestSchema = new Schema ({
	code: String,
	status: {
		type: String,
		default: "Pending"
	},
	quantity: Number,
	requestDate: {
		type: String,
		default: moment(new Date).format("MM/DD/YYYY")
	},
	logs: [
		{
			status: {
				type: String,
				default: "Pending"
			},
			updateDate: {
				type: String,
				default: moment(new Date).format("MM/DD/YYYY")
			},
			message: String
		}
	],
	assetName: String,
	assetId: String,
	requestor: String,
	requestorId: String,
	approver: String,
	approverId: String
})

module.exports = mongoose.model("Request", RequestSchema)