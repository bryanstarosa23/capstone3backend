const mongoose = require ("mongoose");
const Schema = mongoose.Schema;
const moment = require('moment');

const RequestsSchema = new Schema ({
    
    code:String,
    dateRequested:{
        type:String,
        default:moment(Date.now()).format("DD/MM/YYYY")
    },
    requestedVehicles:String,
    requestedVehicleId:String,
    requestedPlateno:String,
    image:String,
    dateNeeded:String,
    untilDate:String,
    location:String,
    status: {
        type:String,
        default: "Pending"
        //Pending, Reserved, Paid, Disapproved,
    },
    logs:[
        {
            status:{
                type: String,
                default: "Pending"
            },
            updateDate:{
                type: String,
                default:moment(Date.now()).format("MM/DD/YYYY")
            },
            message:String
        }
    ],
    
    totalPrice:String,
    downpayment:String,
    balance:String,
    contract:String,
    requestor: String,
    requestorId: String,
    approver: String,
    approverId: String,
})

module.exports = mongoose.model("Requestmodel", RequestsSchema)