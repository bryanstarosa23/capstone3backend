const express = require('express');

const RequestmodelRouter = express.Router();

const RequestsModel = require("../models/Requestmodel");

RequestmodelRouter.post('/addVehicleRequest', async (req, res)=>{
	try{
		let request = RequestsModel({
			code: req.body.code,
			dateRequested:req.body.dateRequested,
			logs:[{message:"Initial Request"}],
			requestedVehicles:req.body.requestedVehicles,
			requestedPlateno:req.body.requestedPlateno,
			requestedVehicleId:req.body.requestedVehicleId,
			image:req.body.image,
			dateNeeded:req.body.dateNeeded,
			untilDate:req.body.untilDate,
			location:req.body.location,
			totalPrice:req.body.totalPrice,
			requestor:req.body.requestor,
			requestorId: req.body.requestorId,
			approver:req.body.approver,
			approverId: req.body.approverId
		});

		request = await request.save();

		res.send(request);
	}catch(e){
		console.log(e)
	}
});

RequestmodelRouter.get("/showvehiclerequests", async (req, res)=>{
	try{
		let requests = await RequestsModel.find();
		res.send(requests);
	}catch(e){
		console.log(e)
	}
}); 

RequestmodelRouter.get('/showvehiclerequestsbyuser/:id', async (req, res)=>{
	let requests = await RequestsModel.find({requestorId:req.params.id});
	res.send(requests);
});

RequestmodelRouter.get('/showvehiclerequestsbyvehicleid/:id', async (req, res)=>{
	let requests = await RequestsModel.find({requestedVehicleId:req.params.id});
	res.send(requests);
});

RequestmodelRouter.patch("/updatevehiclerequeststatus/:id", async (req, res)=>{

	let oldLogs = await RequestsModel.findById(req.params.id);
	let logs = oldLogs.logs
	console.log(logs)
	logs.push(req.body.logs)

	try{
		let condition={_id:req.params.id}
		let update={
			status:req.body.status,
			logs: logs
		}
		let updatedRequest = await RequestsModel.findOneAndUpdate(condition, update, {new:true})
		res.send(updatedRequest);
	}catch(e){
		console.log(e)
	}
})
RequestmodelRouter.delete('/deletevehiclerequest/:id', async(req, res)=>{
	try{
		let deletedRequest = await RequestsModel.findByIdAndDelete(req.params.id);
		res.send(deletedRequest);
	}catch(e){
		console.log(e)
	}
}); 
module.exports = RequestmodelRouter;