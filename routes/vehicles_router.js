const express = require ('express');
const VehicleRouter = express.Router();
const  VehicleModel = require('../models/Vehicle');

VehicleRouter.post('/addvehicle', async(req, res)=>{
    try{
        let vehicle = VehicleModel({
            name:req.body.name,
            description: req.body.description,
            plateNumber: req.body.plateNumber,
            category: req.body.category,
            rentPrice: req.body.rentPrice, 
            image:req.body.image,
            code:req.body.code
        });
        vehicle = await vehicle.save();

        res.send(vehicle);
    }catch(e){
        console.log(e)
    }
})

VehicleRouter.get('/showvehicles', async(req, res)=>{
    try{
        let vehicles = await VehicleModel.find();
        res.send(vehicles);
    }catch(e){
        console.log(e)
    }
})

VehicleRouter.get('/showvehiclebyid/:id', async (req, res)=>{
    try{
        let vehicle = await VehicleModel.findById(req.params.id);
        res.send(vehicle);
    }catch(e){
        console.log(e);
    }
})

VehicleRouter.put('/updatevehiclebyid/:id', async(req, res)=>{
    try{
        let vehicle = await VehicleModel.findById(req.params.id);
        if(!vehicle){
            return res.status(404).send(`Vehicle can't be found`);
        }
        let condition = {_id:req.params.id};
        let updates = {name: req.body.name, description: req.body.description, plateNumber: req.body.plateNumber, category: req.body.category}
        let updatedVehicle = await VehicleModel.findOneAndUpdate(condition, updates, {new:true});
        res.send(updatedVehicle);

    }catch(e){
        console.log(e);
    }
})

VehicleRouter.delete('/deletevehicle/:id', async(req, res)=>{
    try{
        let deletedVehicle = await VehicleModel.findByIdAndDelete(req.params.id);
        res.send(deletedVehicle);
    }catch(e){
        console.log(e)
    }
})

VehicleRouter.patch('/updatevehicledescription/:id', async (req, res)=>{
	try{
		let condition = {_id:req.params.id}
		let update = {description:req.body.description}
		let updated = await VehicleModel.findOneAndUpdate(condition, update, {new:true})
		res.send(updated)
	}catch(e){
		console.log(e)
	}
})

VehicleRouter.patch('/updatevehiclerentprice/:id', async (req, res)=>{
	try{
		let condition = {_id:req.params.id}
		let update = {rentPrice:req.body.rentPrice}
		let updated = await VehicleModel.findOneAndUpdate(condition, update, {new:true})
		res.send(updated)
	}catch(e){
		console.log(e)
	}
})

VehicleRouter.patch('/updatevehiclecategory/:id', async (req, res)=>{
    try{
        let condition = {_id:req.params.id}
        let update = {category:req.body.category}
        let updated = await VehicleModel.findOneAndUpdate(condition, update, {new:true})
        res.send(updated)
    }catch(e){
        console.log(e)
    }
})

VehicleRouter.patch('/updatedescription/:id', async (req, res)=>{
    try{
        let condition = {_id:req.params.id}
        let update = {description:req.body.description}
        let updated = await VehicleModel.findOneAndUpdate(condition, update, {new:true})
        res.send(updated)
    }catch(e){
        console.log(e)
    }
})

VehicleRouter.patch('/updatename/:id', async (req, res)=>{
    try{
        let condition = {_id:req.params.id}
        let update = {name:req.body.name}
        let updated = await VehicleModel.findOneAndUpdate(condition, update, {new:true})
        res.send(updated)
    }catch(e){
        console.log(e)
    }
})

module.exports = VehicleRouter;