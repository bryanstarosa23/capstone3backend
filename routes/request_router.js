const express = require('express');

const RequestRouter = express.Router();

const RequestModel = require("../models/Request");

RequestRouter.post('/addrequest', async (req, res)=>{
	try{
		let request = RequestModel({
			code: req.body.code,
			quantity: req.body.quantity,
			logs:[{message:"Initial Request"}],
			assetName: req.body.assetName,
			assetId: req.body.assetId,
			requestor: req.body.requestor,
			requestorId: req.body.requestorId,
			approver: req.body.approver,
			approverId: req.body.approverId
		});

		request = await request.save();

		res.send(request);
	}catch(e){
		console.log(e)
	}
});

RequestRouter.get("/showrequests", async (req, res)=>{
	try{
		let requests = await RequestModel.find();
		res.send(requests);
	}catch(e){
		console.log(e)
	}
});

RequestRouter.get('/showrequestbyid/:id', async (req, res)=> {
	try{
		let request = await RequestModel.findById(req.params.id);
		res.send(request);
	}catch(e){
		console.log(e)
	}
});

RequestRouter.put('/updaterequest/:id', async (req, res)=>{
	try{
		let request = await RequestModel.findById(req.params.id);

		if(!request){
			return res.status(404).send(`Request can't be found`);
		}

		let condition = {_id:req.params.id};
		let updates = {
			code: req.body.code,
			quantity: req.body.quantity,
			assetName: req.body.assetName,
			assetId: req.body.assetId,
			requestor: req.body.requestor,
			requestorId: req.body.requestorId,
			approver: req.body.approver,
			approverId: req.body.approverId
		}

		let updatedRequest = await RequestModel.findOneAndUpdate(condition, updates, {new:true});

		res.send(updatedRequest);
	}catch(e){
		console.log(e)
	}
});

RequestRouter.delete('/deleterequest/:id', async(req, res)=>{
	try{
		let deletedRequest = await RequestModel.findByIdAndDelete(req.params.id);
		res.send(deletedRequest);
	}catch(e){
		console.log(e)
	}
});

RequestRouter.patch("/updaterequeststatus/:id", async (req, res)=>{

	let oldLogs = await RequestModel.findById(req.params.id);
	let logs = oldLogs.logs
	console.log(logs)
	logs.push(req.body.logs)

	try{
		let condition={_id:req.params.id}
		let update={
			status:req.body.status,
			logs: logs
		}
		let updatedRequest = await RequestModel.findOneAndUpdate(condition, update, {new:true})
		res.send(updatedRequest);
	}catch(e){
		console.log(e)
	}
})

RequestRouter.patch("/updaterequestquantity/:id", async (req, res)=>{

	let oldLogs = await RequestModel.findById(req.params.id);
	let logs = oldLogs.logs
	logs.push(req.body.logs)

	try{
		let condition={_id:req.params.id}
		let update={
			quantity:req.body.quantity,
			logs: logs
		}
		let updatedRequest = await RequestModel.findOneAndUpdate(condition, update, {new:true})
		res.send(updatedRequest);
	}catch(e){
		console.log(e)
	}
})

RequestRouter.get('/showrequestsbyuser/:id', async (req, res)=>{
	let requests = await RequestModel.find({requestorId:req.params.id});
	res.send(requests);
});

module.exports = RequestRouter;