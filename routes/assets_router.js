const express = require('express');

const AssetRouter = express.Router();

const AssetModel = require("../models/Asset");

AssetRouter.post('/addasset', async (req, res)=>{
	try{
		let asset = AssetModel({
			name: req.body.name,
			description: req.body.description,
			serialNumber: req.body.serialNumber,
			category: req.body.category,
			stock: req.body.stock
		});

		asset = await asset.save();

		res.send(asset);
	}catch(e){
		console.log(e)
	}
});

AssetRouter.get("/showassets", async (req, res)=>{
	try{
		let assets = await AssetModel.find();
		res.send(assets);
	}catch(e){
		console.log(e)
	}
});

AssetRouter.get('/showassetbyid/:id', async (req, res)=> {
	try{
		let asset = await AssetModel.findById(req.params.id);
		res.send(asset);
	}catch(e){
		console.log(e)
	}
});

AssetRouter.put('/updateasset/:id', async (req, res)=>{
	try{
		let asset = await AssetModel.findById(req.params.id);

		if(!asset){
			return res.status(404).send(`Asset can't be found`);
		}

		let condition = {_id:req.params.id};
		let updates = {
			name: req.body.name,
			description: req.body.description,
			serialNumber: req.body.serialNumber,
			category: req.body.category,
			stock: req.body.stock
		}

		let updatedAsset = await AssetModel.findOneAndUpdate(condition, updates, {new:true});

		res.send(updatedAsset);
	}catch(e){

	}
});

AssetRouter.delete('/deleteasset/:id', async(req, res)=>{
	try{
		let deletedAsset = await AssetModel.findByIdAndDelete(req.params.id);
		res.send(deletedAsset);
	}catch(e){
		console.log(e)
	}
});

AssetRouter.patch('/updatestock/:id', async (req, res)=>{
	try{
		let condition = {_id:req.params.id}
		let update = {stock:req.body.stock}
		let updated = await AssetModel.findOneAndUpdate(condition, update, {new:true})
		res.send(updated)
	}catch(e){
		console.log(e)
	}
})

AssetRouter.patch('/updatecategory/:id', async (req, res)=>{
	try{
		let condition = {_id:req.params.id}
		let update = {category:req.body.category}
		let updated = await AssetModel.findOneAndUpdate(condition, update, {new:true})
		res.send(updated)
	}catch(e){
		console.log(e)
	}
})



module.exports = AssetRouter;